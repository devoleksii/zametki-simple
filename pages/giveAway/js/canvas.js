const coordinates = {
    xPos: -50,
    yPos: 300,
    radius: 0,
    xChange: 1,
    yChange: 1,
    startAngle: (Math.PI / 180) * 0,
    endAngle: (Math.PI / 180) * 360
};

drawCircle = () => {
    const canvas = document.getElementById("canvas");

    if (canvas) {
        var ctx = canvas.getContext('2d');
        var endXPos = canvas.width - 100;
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        let xPos = coordinates.xPos;
        let yPos = coordinates.yPos;
        let radius = coordinates.radius;
        let xChange = coordinates.xChange;
        let yChange = coordinates.yChange;
        let startAngle = coordinates.startAngle;
        let endAngle = coordinates.endAngle;

        coordinates.xPos = xPos += xChange,
        coordinates.yPos = yPos -= yChange,
        coordinates.radius = radius += 5

        if (xPos > endXPos) {
            coordinates.xPos = -50,
            coordinates.yPos = 300,
            coordinates.radius = 0
        };
        
        ctx.fillStyle = '#FDDB1A';
        ctx.beginPath();
        ctx.arc(xPos, yPos, radius, startAngle, endAngle, true);
        ctx.fill();

        ctx.fillStyle = '#23BDE4';
        ctx.beginPath();
        ctx.arc(xPos - 120, yPos + 120, radius, startAngle, endAngle, true);
        ctx.fill();

        ctx.fillStyle = '#E286F1';
        ctx.beginPath();
        ctx.arc(xPos - 220, yPos + 220, radius, startAngle, endAngle, true);
        ctx.fill();

        ctx.fillStyle = '#fff';
        ctx.beginPath();
        ctx.arc(xPos - 320, yPos + 320, radius, startAngle, endAngle, true);
        ctx.fill();
    }
}