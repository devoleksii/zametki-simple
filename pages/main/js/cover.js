fillLastRequests = () => {
    let requests = JSON.parse(localStorage.getItem('lastRequests'));
    const requestsContainer = document.getElementById('last__requests');
    let request = '';

    if (requests) {
        if (requests[i].length > 40) {
            requests[i] = requests[i].substring(0, 40) + '...';
        }

        for (let i = 0; i < requests.length; i++) {
            request += `<span class="last__request">${requests[i]}</span>`;
        }
    }

    requestsContainer.innerHTML = request;
}

showSearchFieldHandler = () => {
    const cover = document.getElementById('cover');
    const main = document.getElementById('main');
    const feedBack = document.getElementById('feedback');

    cover.className = 'cover-active';

    if (main) {
        setTimeout(() => {
            main.style.display = "none";
        }, 500);
    }

    if (feedBack) {
        setTimeout(() => {
            feedBack.style.display = "none";
        }, 500);
    }

    fillLastRequests();
}

hideSearchFieldHandler = () => {
    const cover = document.getElementById('cover');
    const main = document.getElementById('main');
    const feedBack = document.getElementById('feedback');
    
    cover.className = 'cover-hidden';

    if (main) {
        main.style.display = "flex";
    }

    if (feedBack) {
        feedBack.style.display = "flex";
    }
}

sendSearchRequestHandler = () => {
    let input = document.getElementById('cover-active__search-field');
    let requests = JSON.parse(localStorage.getItem('lastRequests'));
    const requestsContainer = document.getElementById('last__requests');
    let request = '';

    if (requests === null) {
        requests = [];
    }
    
    if (requests !== null && requests.length >= 3 && input.value !== ('' && ' ')) {
        requests.shift();
    }

    if (input.value !== ('' && ' ')) {
        requests.push(input.value);
        console.log(requests);
    }

    for (let i = 0; i < requests.length; i++) {
        request += `<span class="last__request">${requests[i]}</span>`;
    }

    requestsContainer.innerHTML = request;

    input.value = '';

    const serialRequests = JSON.stringify(requests);

    localStorage.setItem('lastRequests', serialRequests);
}

chooseRequestHandler = (request) => {
    const coverInput = document.getElementById('cover-active__search-field');
    var requestsBlock = document.getElementById('last-requests');
    const findedRequests = '';

    coverInput.value = request;
    requestsBlock.innerHTML = findedRequests;
}

getLastRequests = (value) => {
    let requests = JSON.parse(localStorage.getItem('lastRequests'));
    var requestsBlock = document.getElementById('last-requests');
    var findedRequests = '';

    if (value.length === 0) {
        findedRequests = '';
        requestsBlock.innerHTML = findedRequests;
    } else if (requests) {
        for (let i = 0; i < requests.length; i++) {
            if (requests[i].toLowerCase().includes(value.toLowerCase())) {
                requests[i] = requests[i].charAt(0).toUpperCase() + requests[i].slice(1);

                findedRequests += `<span class="cover-active__request" onClick="chooseRequestHandler('${requests[i]}')">${requests[i]}</span>`;
            }
        }
        requestsBlock.innerHTML = findedRequests;
    }
}