window.onload = () => {
    const location = document.getElementById('header__location');
    const button = document.getElementById('popup__decline');
    const mainPopupButton = document.getElementById('popup__decline-main');
    const share = document.getElementById('post__share');
    const showMoreButton = document.getElementById('post__more');
    const showCoverIcon = document.getElementById('header__search-container');
    const hideCoverIcon = document.getElementById('cover-active__close-button');
    const pinIconNotPinned = document.getElementById('post__star-activated-not_pinned');
    const pinIconPinned = document.getElementById('post__star-activated-pinned');
    const goodNotActiveIcon = document.getElementById('post__good-not-active');
    const goodActiveIcon = document.getElementById('post__good-active');
    const badNotActiveIcon = document.getElementById('post__bad-not-active');
    const badActiveIcon = document.getElementById('post__bad-active');
    const popupArrow = document.getElementById('popup__arrow-container');
    const shareButtons = document.getElementsByClassName('popup-share__item');
    const copyButton = document.getElementById('popup-share__item');
    const input = document.getElementById('popup__input-location');
    const cityFirst = document.getElementById('header__city-first');
    const citySecond = document.getElementById('header__city-second');
    const buttonDisabled = document.getElementById('popup__disabled');
    const form = document.getElementsByTagName('form');
    const inputCover = document.getElementById('cover-active__search-field');
    const feedbackLink = document.getElementById('additionally__feedback-link');
    const categoriesIcons = document.getElementsByClassName('categories__icon');
    const locationLink = document.getElementById('is-true-location__link');
    const videoContainer = document.getElementById('post__video');
    const closeVideoButton = document.getElementById('video-popup__active__close');
    
    showMainPopupHandler();

    for (let i = 0; i < form.length; i++) {
        form[i].addEventListener('submit', event => {
            event.preventDefault();
        })
    }

    for (let i = 0; i < shareButtons.length; i++) {
        shareButtons[i].addEventListener('click', () => {
            hidePopupHandler('popup-share share-post');
        });
    }

    location.addEventListener('click', () => {
        showPopupHandler();
    });

    button.addEventListener('click', () => {
        hidePopupHandler('popup popup-header');
    });

    share.addEventListener('click', () => {
        sharePostHandler();
    });

    showMoreButton.addEventListener('click', () => {
        showMoreInfo();
    });

    showCoverIcon.addEventListener('click', () => {
        showSearchFieldHandler();
    });

    hideCoverIcon.addEventListener('click', () => {
        hideSearchFieldHandler();
    });

    pinIconNotPinned.addEventListener('click', () => {
        addToPinHandler();
    });

    pinIconPinned.addEventListener('click', () => {
        removeFromPinHandler();
    });

    goodNotActiveIcon.addEventListener('click', () => {
        makeGoodHandler();
    });

    goodActiveIcon.addEventListener('click', () => {
        makeGoodHandler();
    });

    badActiveIcon.addEventListener('click', () => {
        makeBadHandler();
    });

    badNotActiveIcon.addEventListener('click', () => {
        makeBadHandler();
    });

    popupArrow.addEventListener('click', () => {
        fillFindedCitiesHandler();
    });

    copyButton.addEventListener('click', () => {
        copyUrlHendler('https://google.com');
    });

    input.addEventListener('input', () => {
        findCityHandler();
    });

    buttonDisabled.addEventListener('click', () => {
        if (buttonDisabled.className === 'popup__button popup__accept popup__disabled') {
            return;
        }

        if (choosenCity.length > 0) {
            cityFirst.textContent = choosenCity;
            citySecond.textContent = choosenCity;
            choosenCity = '';
        }

        hidePopupHandler('popup popup-header');
    });

    inputCover.addEventListener('keydown', event => {
        if (event.keyCode === 13) {
            sendSearchRequestHandler();
        };
    });

    inputCover.addEventListener('input', () => {
        getLastRequests(inputCover.value);
    })

    feedbackLink.addEventListener('click', () => {
        hideSearchFieldHandler();
    });

    for (let i = 0; i < categoriesIcons.length; i++) {
        categoriesIcons[i].addEventListener('click', () => {
            hideSearchFieldHandler()
        })
    }

    mainPopupButton.addEventListener('click', () => {
        hideMainPopupHandler();
    })

    document.addEventListener('click', event => {
        const modal = document.getElementById('is-true-location');
        const locationModalRect = document.getElementsByClassName('is-true-location__rect');
        const locationModalQuestion = document.getElementsByClassName('is-true-location__question');
        const locationModalLink = document.getElementsByClassName('is-true-location__link');

        if (event.target !== modal &&
            event.target !== locationModalRect[0] &&
            event.target !== locationModalQuestion[0] &&
            event.target !== locationModalLink[0]) {
            modal.style.display = 'none';
        }
    })

    locationLink.addEventListener('click', () => {
        const modal = document.getElementById('is-true-location');

        showPopupHandler();

        modal.style.display = 'none';
    })

    videoContainer.addEventListener('click', () => {
        showPopupVideoHandler();
    })

    closeVideoButton.addEventListener('click', () => {
        hidePopupVideoHandler();
    })
}