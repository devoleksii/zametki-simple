showMoreInfo = () => {
    const showMore = document.getElementById(`post__more`);
    const button = document.getElementById(`post__coupon`);
    const stocks = document.getElementById(`post__stocks`);

    showMore.style.display = 'none';
    button.style.display = 'block';
    button.style.maxHeight = button.scrollHeight + 'px';
    
    if (stocks) {
        stocks.style.display = 'flex';
        stocks.style.maxHeight = stocks.scrollHeight + 'px';
    }
}

addToPinHandler = () => {
    const pinIconNotPinned = document.getElementById('post__star-activated-not_pinned');
    const pinIconPinned = document.getElementById('post__star-activated-pinned');

    pinIconPinned.style.display = 'block';
    pinIconNotPinned.style.display = 'none';
}

removeFromPinHandler = () => {
    const pinIconNotPinned = document.getElementById('post__star-activated-not_pinned');
    const pinIconPinned = document.getElementById('post__star-activated-pinned');

    pinIconPinned.style.display = 'none';
    pinIconNotPinned.style.display = 'block';
}

makeGoodHandler = () => {
    const goodNotActiveIcon = document.getElementById('post__good-not-active');
    const goodActiveIcon = document.getElementById('post__good-active');
    const badNotActiveIcon = document.getElementById('post__bad-not-active');
    const badActiveIcon = document.getElementById('post__bad-active');
    const goodNotActiveStyle = getComputedStyle(goodNotActiveIcon);
    const badActiveStyle = getComputedStyle(badActiveIcon);

    if (goodNotActiveStyle.display === 'none') {
        goodActiveIcon.style.display = 'none';
        goodNotActiveIcon.style.display = 'block';
    } else {
        if (badActiveStyle.display === 'block') {
            badActiveIcon.style.display = 'none';
            badNotActiveIcon.style.display = 'block';
        }
        
        goodActiveIcon.style.display = 'block';
        goodNotActiveIcon.style.display = 'none';
    }

}

makeBadHandler = () => {
    const badNotActiveIcon = document.getElementById('post__bad-not-active');
    const badActiveIcon = document.getElementById('post__bad-active');
    const goodNotActiveIcon = document.getElementById('post__good-not-active');
    const goodActiveIcon = document.getElementById('post__good-active');
    const badNotActiveStyle = getComputedStyle(badNotActiveIcon);
    const goodActiveStyle = getComputedStyle(goodActiveIcon);

    if (badNotActiveStyle.display === 'none') {
        badActiveIcon.style.display = 'none';
        badNotActiveIcon.style.display = 'block';
    } else {
        if (goodActiveStyle.display === 'block') {
            goodActiveIcon.style.display = 'none';
            goodNotActiveIcon.style.display = 'block';
        }

        badActiveIcon.style.display = 'block';
        badNotActiveIcon.style.display = 'none';
    }
}

function getTimeRemaining(endtime) {
    const t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));

    return {
      'total': t,
      'days': days,
      'hours': hours,
      'minutes': minutes,
      'seconds': seconds
    };
  }

   
function initializeClock(id, endtime) {
const clock = document.getElementById(id);

if (clock) {
    const daysSpan = clock.querySelector('.days');
    const hoursSpan = clock.querySelector('.hours');
    const minutesSpan = clock.querySelector('.minutes');
    const secondsSpan = clock.querySelector('.seconds');
    
    function updateClock() {
        const t = getTimeRemaining(endtime);

        if (t.days < 10) {
            daysSpan.innerHTML = '0' + t.days;
        } else {
            daysSpan.innerHTML = t.days;
        }

        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            daysSpan.innerHTML = '00';
            hoursSpan.innerHTML = '00';
            minutesSpan.innerHTML = '00';
            secondsSpan.innerHTML = '00';
            clearInterval(timeinterval);
        }
    }
    
    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}
}

const deadline = "Wed Sep 17 2019 11:26:45 GMT+0300";
initializeClock('countdown', deadline);