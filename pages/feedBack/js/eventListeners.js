window.onload = () => {
    const location = document.getElementById('header__location');
    const button = document.getElementById('popup__decline');
    const showCoverIcon = document.getElementById('header__search-container');
    const hideCoverIcon = document.getElementById('cover-active__close-button');
    const popupArrow = document.getElementById('popup__arrow-container');
    const shareButtons = document.getElementsByClassName('popup-share__item');
    const input = document.getElementById('popup__input-location');
    const cityFirst = document.getElementById('header__city-first');
    const citySecond = document.getElementById('header__city-second');
    const buttonDisabled = document.getElementById('popup__disabled');
    const form = document.getElementsByTagName('form');
    const inputCover = document.getElementById('cover-active__search-field');
    const feedbackLink = document.getElementById('additionally__feedback-link');
    const categoriesIcons = document.getElementsByClassName('categories__icon');

    for (let i = 0; i < form.length; i++) {
        form[i].addEventListener('submit', event => {
            event.preventDefault();
        })
    }

    for (let i = 0; i < shareButtons.length; i++) {
        shareButtons[i].addEventListener('click', () => {
            hidePopupHandler('popup-share share-post');
        });
    }

    location.addEventListener('click', () => {
        showPopupHandler();
    });

    button.addEventListener('click', () => {
        hidePopupHandler('popup popup-header');
    });

    showCoverIcon.addEventListener('click', () => {
        showSearchFieldHandler();
    });

    hideCoverIcon.addEventListener('click', () => {
        hideSearchFieldHandler();
    });

    popupArrow.addEventListener('click', () => {
        fillFindedCitiesHandler();
    });

    input.addEventListener('input', () => {
        findCityHandler();
    });

    buttonDisabled.addEventListener('click', () => {
        if (buttonDisabled.className === 'popup__button popup__accept popup__disabled') {
            return;
        }

        if (choosenCity.length > 0) {
            cityFirst.textContent = choosenCity;
            citySecond.textContent = choosenCity;
            choosenCity = '';
        }

        hidePopupHandler('popup popup-header');
    });

    inputCover.addEventListener('keydown', event => {
        if (event.keyCode === 13) {
            sendSearchRequestHandler();
        };
    });

    inputCover.addEventListener('input', () => {
        getLastRequests(inputCover.value);
    })

    feedbackLink.addEventListener('click', () => {
        hideSearchFieldHandler();
    });

    for (let i = 0; i < categoriesIcons.length; i++) {
        categoriesIcons[i].addEventListener('click', () => {
            hideSearchFieldHandler()
        })
    }
}