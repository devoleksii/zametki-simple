fillLastRequests = () => {
    let requests = JSON.parse(localStorage.getItem('lastRequests'));
    const requestsContainer = document.getElementById('last__requests');
    let request = '';

    if (requests) {
        for (let i = 0; i < requests.length; i++) {
            if (requests[i].length > 40) {
                requests[i] = requests[i].substring(0, 40) + '...';
            }

            request += `<span class="last__request">${requests[i]}</span>`;
        }
    }

    requestsContainer.innerHTML = request;
}

showSearchFieldHandler = () => {
    const cover = document.getElementById('cover');
    const main = document.getElementsByClassName('company');

    cover.className = 'cover-active';

    if (main) {
        setTimeout(() => {
            main[0].style.display = "none";
        }, 250);
    }

    fillLastRequests();
}

hideSearchFieldHandler = () => {
    const cover = document.getElementById('cover');
    const main = document.getElementsByClassName('company');
    
    cover.className = 'cover-hidden';

    if (main) {
        main[0].style.display = "flex";
    }
}

sendSearchRequestHandler = () => {
    let input = document.getElementById('cover-active__search-field');
    let requests = JSON.parse(localStorage.getItem('lastRequests'));
    const requestsContainer = document.getElementById('last__requests');
    let request = '';

    if (requests === null) {
        requests = [];
    }
    
    if (requests !== null && requests.length >= 3 && input.value !== ('' && ' ')) {
        requests.shift();
    }

    if (input.value !== ('' && ' ')) {
        requests.push(input.value);
    }

    for (let i = 0; i < requests.length; i++) {
        request += `<span class="last__request">${requests[i]}</span>`;
    }

    requestsContainer.innerHTML = request;

    input.value = '';

    const serialRequests = JSON.stringify(requests);

    localStorage.setItem('lastRequests', serialRequests);
}

chooseRequestHandler = (request) => {
    const coverInput = document.getElementById('cover-active__search-field');
    var requestsBlock = document.getElementById('last-requests');
    const findedRequests = '';

    coverInput.value = request;
    requestsBlock.innerHTML = findedRequests;
}

getLastRequests = (value) => {
    let requests = JSON.parse(localStorage.getItem('lastRequests'));
    var requestsBlock = document.getElementById('last-requests');
    var findedRequests = '';

    if (value.length === 0) {
        findedRequests = '';
        requestsBlock.innerHTML = findedRequests;
    } else if (requests) {
        for (let i = 0; i < requests.length; i++) {
            if (requests[i].toLowerCase().includes(value.toLowerCase())) {
                requests[i] = requests[i].charAt(0).toUpperCase() + requests[i].slice(1);

                findedRequests += `<span class="cover-active__request" onClick="chooseRequestHandler('${requests[i]}')">${requests[i]}</span>`;
            }
        }
        requestsBlock.innerHTML = findedRequests;
    }
}