const cityes = [
    'Вінниця',
    'Дніпро',
    'Донецьк',
    'Житомир',
    'Запоріжжя',
    'Київ',
    'Краматорськ',
    'Луганськ',
    'Луцьк',
    'Львів',
    'Миколаїв',
    'Одеса',
    'Полтава',
    'Рівне',
    'Суми',
    'Тернопіль',
    'Ужгород',
    'Харків',
    'Херсон',
    'Хмельницький',
    'Черкаси',
    'Чернівці',
    'Чернігів'
]

const defaultCity = 'київ';
let choosenCity = '';

showPopupHandler = () => {
    const popup = document.getElementById('popup popup-header');
    const bgLayer = document.getElementById('main__bg-layer');

    popup.className = 'popup';
    bgLayer.className = 'main__bg-layer';
}

hidePopupHandler = (id) => {
    const popup = document.getElementById(id);
    const bgLayer = document.getElementById('main__bg-layer');
    const input = document.getElementsByTagName('input');
    const button = document.getElementById('popup__disabled');
    const activeModal = document.getElementsByClassName("popup");
    const hintContaierMain = document.getElementById('input__hint-container-hidden');
    const hintContaier = document.getElementById('input__hint-hidden');
    const locationArrow = document.getElementById('popup__location-arrow');

    button.className = 'popup__button popup__accept popup__disabled';
    popup.className = 'popup-hidden';

    for (let i = 0; i < input.length; i++) {
        input[i].value = '';
    };

    if (hintContaierMain.className === 'input__hint-container-active') {
        locationArrow.classList.remove('popup__arrow');
        locationArrow.classList.add('popup__arrow-up');
        hintContaierMain.className = 'input__hint-container';
        hintContaier.className = 'input__hint';
    }
    
    findedCityes = '';
    hintContaier.innerHTML = findedCityes;

    if (activeModal.length > 0) {
        return;
    }

    setTimeout(() => {
        bgLayer.className = 'main__bg-layer-hidden';
    }, 300);
}
    
sharePostHandler = () => {
    const popup = document.getElementById('popup-share share-post');
    const bgLayer = document.getElementById('main__bg-layer');

    popup.className = 'popup-share popup';
    bgLayer.className = 'main__bg-layer';
}

fillFindedCitiesHandler = () => {
    const hintContaierMain = document.getElementById('input__hint-container-hidden');
    const hintContaier = document.getElementById('input__hint-hidden');
    const locationArrow = document.getElementById('popup__location-arrow');

    let findedCityes = '';

    if (locationArrow.classList.value === 'popup__arrow-up') {
        locationArrow.classList.add('popup__arrow');
        locationArrow.classList.remove('popup__arrow-up');
        hintContaierMain.className = 'input__hint-container-active';
        hintContaier.className = 'input__hint-active';

        for (let i = 0; i < cityes.length; i++) {
            findedCityes += `<span class="input__city" onClick="chooseCityHandler('${cityes[i]}')">${cityes[i]}</span>`;
        }
    } else {
        locationArrow.classList.remove('popup__arrow');
        locationArrow.classList.add('popup__arrow-up');
        hintContaierMain.className = 'input__hint-container';
        hintContaier.className = 'input__hint';

        findedCityes = '';
    }
    
    hintContaier.innerHTML = findedCityes;
}

findCityHandler = () => {
    const input = document.getElementById('popup__input-location');
    const hintContaierMain = document.getElementById('input__hint-container-hidden');
    const hintContaier = document.getElementById('input__hint-hidden');
    const locationArrow = document.getElementById('popup__location-arrow');
    const button = document.getElementById('popup__disabled');
    let findedCityes = '';

    if (input.value.length === 0) {
        locationArrow.classList.remove('popup__arrow');
        locationArrow.classList.add('popup__arrow-up');
        hintContaierMain.className = 'input__hint-container';
        hintContaier.className = 'input__hint';
        hintContaier.innerHTML = findedCityes;
        findedCityes = '';
        return;
    }

    for (let i = 0; i < cityes.length; i++) {
        if (cityes[i].toLowerCase() === event.target.value.toLowerCase()) {
            event.target.value = cityes[i];
            button.className = 'popup__button popup__accept';
            locationArrow.classList.remove('popup__arrow');
            locationArrow.classList.add('popup__arrow-up');
            hintContaierMain.className = 'input__hint-container';
            hintContaier.className = 'input__hint';
            hintContaier.innerHTML = findedCityes;
            choosenCity = cityes[i];
            findedCityes = '';

            i = Number.MAX_VALUE;

            return;
        }

        else if (cityes[i].toLowerCase() !== event.target.value.toLowerCase()) {
            button.className = 'popup__button popup__accept popup__disabled';
        }
    }
    
    for (let i = 0; i < cityes.length; i++) {
        if (cityes[i].toLowerCase().includes(input.value.toLowerCase())) {
            findedCityes += `<span class="input__city" onClick="chooseCityHandler('${cityes[i]}')">${cityes[i]}</span>`;
        }
    }

    if (findedCityes.length > 0) {
        locationArrow.classList.add('popup__arrow');
        locationArrow.classList.remove('popup__arrow-up');
        hintContaierMain.className = 'input__hint-container-active';
        hintContaier.className = 'input__hint-active';
    }

    hintContaier.innerHTML = findedCityes;
}

chooseCityHandler = city => {
    const input = document.getElementById('popup__input-location');
    const hintContaierMain = document.getElementById('input__hint-container-hidden');
    const hintContaier = document.getElementById('input__hint-hidden');
    const locationArrow = document.getElementById('popup__location-arrow');
    const button = document.getElementById('popup__disabled');

    button.className = 'popup__button popup__accept';
    locationArrow.classList.remove('popup__arrow');
    locationArrow.classList.add('popup__arrow-up');
    hintContaierMain.className = 'input__hint-container';
    hintContaier.className = 'input__hint';
    input.value = city;
    choosenCity = city;
}

copyUrlHendler = (url) => {
    navigator.clipboard.writeText(url);
}

hideMainPopupHandler = () => {
    const popupMain = document.getElementById('popup popup-main');
    const bgLayer = document.getElementById('main__bg-layer');
    const activeModal = document.getElementsByClassName("popup");
    
    popupMain.className = 'popup-hidden';

    if (activeModal.length > 0) {
        return;
    }

    setTimeout(() => {
        bgLayer.className = 'main__bg-layer-hidden';
    }, 300);
}

showPopupVideoHandler = () => {
    const popup = document.getElementById('post__video-popup');
    const layer = document.getElementById('main__bg-layer');

    popup.className = 'video-popup__active popup';
    layer.className = 'main__bg-layer';
}

hidePopupVideoHandler = () => {
    const popup = document.getElementById('post__video-popup');
    const layer = document.getElementById('main__bg-layer');

    popup.className = 'video-popup__hidden';

    const activeModal = document.getElementsByClassName("popup");
    
    if (activeModal.length > 0) {
        return;
    }

    setTimeout(() => {
        layer.className = 'main__bg-layer-hidden';
    }, 300);
}